CREATE TABLE usuario (
  id BIGINT AUTO_INCREMENT PRIMARY KEY,
  email VARCHAR(100) NOT NULL UNIQUE
);

CREATE TABLE reparador (
  id BIGINT AUTO_INCREMENT PRIMARY KEY,
  email VARCHAR(100) NOT NULL UNIQUE
);

CREATE TABLE  profesion (
  id BIGINT AUTO_INCREMENT PRIMARY KEY,
  nombre VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE profesion_reparador (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    id_reparador BIGINT NOT NULL,
    id_profesion BIGINT NOT NULL,
    FOREIGN KEY (id_reparador) REFERENCES reparador(id),
    FOREIGN KEY (id_profesion) REFERENCES profesion(id),
    UNIQUE KEY unique_id_profesion_id_reparador (id_profesion, id_reparador)
);

CREATE TABLE trabajo (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    titulo VARCHAR(255) NOT NULL,
    descripcion VARCHAR(3000) NOT NULL,
    token VARCHAR(255) NOT NULL UNIQUE,
    id_usuario BIGINT NOT NULL,
    id_profesion BIGINT NOT NULL,
    FOREIGN KEY (id_usuario) REFERENCES usuario(id),
    FOREIGN KEY (id_profesion) REFERENCES profesion(id)
);

