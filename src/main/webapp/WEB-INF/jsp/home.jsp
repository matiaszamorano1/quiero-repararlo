<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- BANNER : begin -->
<div id="banner">

    <!-- BANNER BG : begin -->
    <div class="banner-bg">
        <div id="qr-js-primero" class="banner-bg-item"><img src="img/fondos/mecanico.jpg" alt="" class="hidden-lg"></div>
        <div class="banner-bg-item"><img src="img/fondos/painter3.jpg" alt="" class="hidden-lg"></div>
        <div class="banner-bg-item"><img src="img/fondos/carpintero.jpg" alt="" class="hidden-lg"></div>
        <div class="banner-bg-item"><img src="img/fondos/computadora.jpg" alt="" class="hidden-lg"></div>
        <div class="banner-bg-item"><img src="img/fondos/cables.jpg" alt="" class="hidden-lg"></div>
        <div class="banner-bg-item"><img src="img/fondos/cocina.jpg" alt="" class="hidden-lg"></div>
    </div>
    <!-- BANNER BG : end -->

    <!-- BANNER SEARCH : begin -->
    <div id="presentacion" class="banner-search">
        <div class="container">
            <div class="banner-search-inner">
                <ul class="custom-list tab-title-list clearfix">
                    <li class="tab-title active"><a href="#">�Sabes reparar?</a></li>
                </ul>
                <div class="tab-content-list">

                    <div id="signup">
                        <!--p>
                            <strong>�Sab�s reparar?</strong> 
                            
                        </p-->
                        <div id="qr-js-mensaje-suscripcion" class="testimonial textalign-center">
                            <blockquote class="quote">
                                Sumate y te regalamos una cuenta gratis en el lanzamiento
                                <footer><cite>Presuscripci�n exclusiva</cite></footer>
                            </blockquote>
                        </div>
                        <div id="success" class="hidden-lg testimonial textalign-center">
                            <blockquote class="quote">
                                Gracias por sumarte. Nos contactaremos cuando inaguremos el sitio!
                                <footer><cite>Lanzamos en unas semanas!</cite></footer>
                            </blockquote>
                        </div>
                        <form id="qr-form" class="validate default-form">
                            <span class="search-input qr-email-input">
                                <input type="text" id="qr-email" placeholder="Tu email..."  required>
                            </span>
                            <span class="select-box qr-profesiones-home" title="Profesiones">
                                <select name="profesiones" data-placeholder="�Qu� sabes reparar?">
                                    <option>�Qu� sabes reparar?</option>
                                    <c:forEach items="${profesiones}" var="profesion">
                                        <option value="${profesion.id}">${profesion.nombre}</option>
                                    </c:forEach>
                                </select>
                            </span>
                            <span class="submit-btn">
                                <button class="button" type="submit"> Sumate!</button>
                            </span>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- BANNER SEARCH : end -->

</div>
<!-- BANNER : end -->

<!-- CORE : begin -->
<div id="core">

    <!-- CONTENT SECTION - SERVICES : begin -->
    <section class="content-section services">
        <div class="container">
            <div class="row">
                <div class="col-sm-4"> 

                    <!-- SERVICE 1 : begin -->
                    <div class="service-container">
                        <p class="service-icon"><i class="fa fa-group"></i></p>
                        <h3 class="service-title">M�s Clientes</h3>
                        <p>Vas a recibir ofertas de trabajo en el momento en que lo necesiten.</p>
                    </div>
                    <!-- SERVICE 1 : end -->

                </div>
                <div class="col-sm-4">

                    <!-- SERVICE 2 : begin -->
                    <div class="service-container">
                        <p class="service-icon"><i class="fa fa-calculator"></i></p>
                        <h3 class="service-title">Presupuestos</h3>
                        <p>Arm� el presupuesto seg�n tu experiencia de forma sencilla y clara.</p>
                    </div>
                    <!-- SERVICE 2 : end -->

                </div>
                <div class="col-sm-4">

                    <!-- SERVICE 3 : begin -->
                    <div class="service-container">
                        <p class="service-icon"><i class="fa fa-lock"></i></p>
                        <h3 class="service-title">Confianza</h3>
                        <p>Nuestro sistema de reputaci�n te permitir� hacer valer tu buen servicio con los clientes.</p>
                    </div>
                    <!-- SERVICE 3 : end -->

                </div>
            </div>
        </div>
    </section>
    <!-- CONTENT SECTION - SERVICES : end -->

    <!-- BROWSE : begin -->
    <section id="browse">
        <div class="container">
            <h2>Reparadores disponibles</h2>
            <div class="row">
                <c:forEach items="${resumenReparadores}" var="resumen">
                    <div class="col-sm-4">
                        <div data-percentage="${(resumen.cantidad < 10) ? resumen.cantidad * 10 : 100}" class="progress-bar">
                            <h5 class="progress-bar-title">${resumen.profesion} (${resumen.cantidad})</h5>
                            <div class="progress-bar-inner"><span></span></div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </section>
    <!-- BROWSE : end -->

    <!-- CONTENT SECTION: begin -->
    <section class="content-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <h2>Acerca de</h2>
                    <p>Quiero Repararlo es la comunidad para conseguir el mejor presupuesto y servicio para tus reparaciones!</p>
                    <p>Y si tu profesi�n es reparar autos, electrodom�sticos, computadoras o lo que sea, Quiero Repararlo es la herramienta que te permitir� 
                        multiplicar tus clientes y hacer valer tu buen trabajo.</p>
                    <p class="cta-button">
                        <a href="#" class="button"><i class="fa fa-heart"></i> Sumate!</a>
                    </p>

                </div>
                <div class="col-sm-4">

                    <p><img src="img/reparadorRojo.png" alt="" class="qr-qhome"></p>

                </div>
            </div>
        </div>
    </section>
    <!-- CONTENT SECTION : end -->

</div>
<!-- CORE : end -->
