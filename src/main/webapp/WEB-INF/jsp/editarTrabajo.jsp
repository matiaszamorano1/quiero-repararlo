<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="page-property-details" id="core">

    <div class="page-header">
        <div class="container">
            <div class="page-header-inner clearfix">
                <h1>${trabajo.profesion.nombre}</h1>
            </div>
        </div>
    </div>

    <div class="main-wrapper-container">
        <div class="container">
            <div id="main-wrapper">
                <div class="row">
                    <div class="col-md-8 col-md-push-4">

                        <div class="property-details">

                            <div class="row">
                                <div class="col-md-12">

                                    <div class="property-description">

                                        <div class="description-text">
                                            <h4>Titulo: <span id="tituloTrabajo" class="qr-js-trabajo-editable" data-type="text" data-title="Título del trabajo"><c:out value="${trabajo.titulo}"/></span></h4>
                                            <h4>Descripción:</h4>
                                            <p>
                                                <span href="#" id="descripcionTrabajo" data-type="textarea" data-title="Descripción del trabajo a realizar" class="qr-js-trabajo-editable"><c:out value="${trabajo.descripcion}"/></span>
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-md-pull-8">
                        <aside class="property-location-details">
                            <h3 class="property-location-title">Contacto</h3>

                            <div class="property-info">
                                <h4 class="property-info-title">Información</h4>

                                <div class="toggle-container property-address">
                                    <h5 class="toggle-title">Detalles</h5>
                                    <div class="toggle-content">

                                        <p>Email: ${trabajo.usuario.email}<br>
                                        </p>

                                    </div>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>