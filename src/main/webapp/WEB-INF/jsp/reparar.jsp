<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- BANNER : begin -->
<div id="banner">

    <!-- BANNER BG : begin -->
    <div class="banner-bg">
        <div class="banner-bg-item"><img src="img/bg2.png" alt=""></div>
    </div>
    <!-- BANNER BG : end -->

    <!-- BANNER SEARCH : begin -->
    <div id="presentacion" class="banner-search">
        <div class="container">
            <div class="banner-search-inner">
                <ul class="custom-list tab-title-list clearfix">
                    <li class="tab-title active"><a href="#">�Qu� necesitas reparar?</a></li>
                </ul>
                <div class="tab-content-list">

                    <div id="signup">
                        <div id="qr-js-mensaje-suscripcion" class="testimonial textalign-center">
                            <blockquote class="quote">
                                Indic� que necesitas reparar y recibir�s presupuestos para elegir el m�s conveniente
                            </blockquote>
                        </div>
                        <form id="qr-form-reparar" class="validate default-form">
                            <span class="search-input qr-email-input">
                                <input type="text" id="qr-email" placeholder="Tu email..."  required>
                            </span>
                            <span class="select-box qr-profesiones-home" title="Profesiones">
                                <select name="profesiones" data-placeholder="Necesito un especialista en...">
                                    <option>�Qu� sabes reparar?</option>
                                    <c:forEach items="${profesiones}" var="profesion">
                                        <option value="${profesion.id}">${profesion.nombre}</option>
                                    </c:forEach>
                                </select>
                            </span>
                            <span class="submit-btn">
                                <button class="button" type="submit"> Siguiente</button>
                            </span>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- BANNER SEARCH : end -->

</div>
<!-- BANNER : end -->