<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html lang="es">
    <head>
        <c:set var="root" scope="request">${pageContext.request.contextPath}</c:set>
        <c:set var="url">${pageContext.request.requestURL}</c:set>
        <base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />

        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>QuieroRepararlo.com</title>
        <link rel="shortcut icon" href="img/favicon.png" />  

        <!-- GOOGLE FONTS : begin -->
        <link href="http://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet" type="text/css">
        <!-- GOOGLE FONTS : end -->

        <!-- STYLESHEETS : begin -->
        <!--link rel="stylesheet" type="text/css" href="css/bootstrap.css"-->
        <link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <!-- Optional theme -->
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="css/animate.custom.css">
        <link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/skins/default.css">

        <link rel="stylesheet" href="css/main.css"  />
        <!-- STYLESHEETS : end -->

        <!--[if lte IE 8]>
                        <link rel="stylesheet" type="text/css" href="css/oldie.css">
                        <script src="js/respond.min.js" type="text/javascript"></script>
        <![endif]-->
        <script src="js/modernizr.custom.min.js" type="text/javascript"></script>
    </head>
    <body class="enable-fixed-header enable-style-switcher enable-inview-animations">
        <!-- HEADER : begin -->
        <header id="header">
            <div class="container">
                <div class="header-inner clearfix">

                    <!-- HEADER BRANDING : begin -->
                    <div class="header-branding">
                        <a href="${root}"><img src="img/brandRosa.png" alt="Quiero Repararlo"></a>
                    </div>
                    <!-- HEADER BRANDING : end -->

                    <!-- HEADER NAVBAR : begin -->
                    <div class="header-navbar">
                        <div class="header-tools">



                            <!-- HEADER LOGIN : begin -->
                            <div class="header-login">
                                <button class="login-toggle header-btn"><i class="fa fa-power-off"></i> Login</button>
                            </div>
                            <!-- HEADER LOGIN : end -->

                            <!-- HEADER ADD OFFER : begin -->
                            <span class="header-add-offer"><a class="button" href="reparar"><i class="fa fa-plus"></i> Publicar Reparación</a></span>
                            <!-- HEADER ADD OFFER : end -->

                        </div>
                    </div>
                    <!-- HEADER NAVBAR : end -->


                </div>
            </div>
        </header>
        <!-- HEADER : end -->

        <tiles:insertAttribute name="body" />


        <!-- FOOTER : begin -->
        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">

                        <!-- FOOTER TEXT : begin -->
                        <p>Copyright 2014 &copy; Quiero Repararlo. Todos los derechos reservados.</p>
                        <!-- FOOTER TEXT : end -->

                    </div>
                    <div class="col-sm-4">

                        <!-- FOOTER SOCIAL : begin -->
                        <ul class="footer-social custom-list">
                            <li><a href="https://www.facebook.com/quierorepararlo" title="Facebook" target="_blank"><i class="fa fa-facebook-square"></i><span>Facebook</span></a></li>
                            <li><a href="https://twitter.com/quierorepararlo" title="Twitter" target="_blank"><i class="fa fa-twitter-square"></i><span>Twitter</span></a></li>
                            <li><a href="https://plus.google.com/112402547754501870508" title="Google+" target="_blank"><i class="fa fa-google-plus-square"></i><span>Google+</span></a></li>
                            <li><a href="http://www.linkedin.com/company/quiero-repararlo" title="LinkedIn" target="_blank"><i class="fa fa-linkedin-square"></i><span>LinkedIn</span></a></li>
                        </ul>

                        <!-- FOOTER SOCIAL : end -->

                    </div>
                </div>
            </div>
        </footer>
        <!-- FOOTER : end -->

        <!-- SCRIPTS : begin -->
        <!-- Latest compiled and minified JavaScript -->
        <script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>
        <script src="js/jquery.ba-outside-events.min.js" type="text/javascript"></script>
        <script src="js/jquery.inview.min.js" type="text/javascript"></script>
        <!--[if lte IE 8]>
        <script src="js/jquery.placeholder.js" type="text/javascript"></script>
        <![endif]-->
        <script src="js/owl.carousel.min.js" type="text/javascript"></script>
        <script src="js/jquery.magnific-popup.min.js" type="text/javascript"></script>
        <script src="twitter/jquery.tweet.min.js" type="text/javascript"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
        <script src="js/scripts.js" type="text/javascript"></script>
        <!-- SCRIPTS : end -->

        <script src="js/app/quierorepararlo.js"></script>
        <script src="js/app/service/service.js"></script>
        <script src="js/app/service/profesionreparador/profesionreparador.js"></script>
        <script src="js/app/service/trabajo/trabajo.js"></script>
        <script src="js/app/ui/ui.js"></script>

        <tiles:importAttribute name="js" scope="page"/>
        <c:if test="${not empty js}">
            <script src="${js}"></script>
        </c:if>
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-50959442-1', 'quierorepararlo.com');
            ga('send', 'pageview');

        </script>

    </body>
</html>
