quierorepararlo.service.profesionreparador = (function() {

    function guardar(profesionreparador) {
        var url = quierorepararlo.service.url() + "profesionreparador";
        return quierorepararlo.service.post(url, profesionreparador);
    }

    return {
        guardar: guardar
    };
})();