quierorepararlo.service.trabajo = (function() {

    function guardar(trabajo) {
        var url = quierorepararlo.service.url() + "trabajo";
        return quierorepararlo.service.post(url, trabajo);
    }

    return {
        guardar: guardar
    };
})();