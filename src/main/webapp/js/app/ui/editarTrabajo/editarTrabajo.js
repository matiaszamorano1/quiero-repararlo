quierorepararlo.ui.editarTrabajo = (function() {

    var MENSAJE_SIN_DATOS = "Click para editar";

    function init() {
        $(".qr-js-trabajo-editable").editable({
            emptytext: MENSAJE_SIN_DATOS,
            success: function(response, newValue) {
                var type = $(this).data("type");
                if (type === "combodate") {
                    newValue = newValue.format("DD/MM/YYYY");
                }
                $(this).text(newValue);
                actualizarPersona();
            },
            validate: function(value) {
                var obligatorio = $(this).data("obligatorio");
                if (obligatorio) {
                    if (value === null || value === '') {
                        return 'El campo es obligatorio';
                    }
                }
            }
        });
    }

    return {
        init: init
    };
})();

$(document).ready(function() {
    quierorepararlo.ui.editarTrabajo.init();
});
