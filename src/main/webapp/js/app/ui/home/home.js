quierorepararlo.ui.home = (function() {
    function init() {
        $('#qr-form').submit(guardarReparador);
        inicializarBackground();
    }

    function inicializarBackground() {
        var imagenes = $('#banner .banner-bg').find(".banner-bg-item");
        var cantidad = imagenes.length;
        var indice = Math.floor((Math.random() * cantidad) + 1);

        for (var i = 0; i < cantidad; i++) {
            var img = imagenes.eq(i).find('img');
            if (i !== indice) {
                img.hide();
            } else {
                $("#qr-js-primero").css('background-image', 'url(' + img.attr('src') + ')');
            }
        }
        $("#qr-js-primero").fadeTo("slow", 1);
    }

    function guardarReparador() {
        var profesionReparador = {};
        var reparador = {};
        var profesion = {};
        reparador.email = $('#qr-email').val().trim().toLowerCase();
        profesion.id = $('select[name=profesiones] option:selected').val();

        profesionReparador.profesion = profesion;
        profesionReparador.reparador = reparador;

        quierorepararlo.service.profesionreparador.guardar(profesionReparador)
                .done(function() {
                    $('#qr-email').val("");
                    $('#qr-form').hide();
                    $("#success").removeClass("hidden-lg");
                    $('#qr-js-mensaje-suscripcion').addClass("hidden-lg");
                })
                .fail(function(e) {
                    alert("Ocurrio un error. ¿Quizás ya te habías anotado?");
                });
        return false;
    }

    return {
        init: init
    };
})();

$(document).ready(function() {
    quierorepararlo.ui.home.init();
});