quierorepararlo.ui = (function() {

    /**
     * Denmora la ejeuccion de una funcion una cierta cantidad de tiempo (en
     * milisegundos). Si se realiza otra invocacion antes de este tiempo,
     * se cancela la primer invocación, se resetea el timer y se pone la nueva
     * invocacion en espera.
     * Por ejemplo, es útil para "ejecutar una búsqueda 2 segundos después del
     * ultimo keypress".
     * Leer mas: http://stackoverflow.com/questions/4364729/jquery-run-code-2-seconds-after-last-keypress
     *
     * @param f la funcion a invocar.
     * @param delay la demora en milisegundos. Default: 500.
     */
    function throttle(f, delay) {
        var timer = null;
        return function() {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = window.setTimeout(function() {
                f.apply(context, args);
            },
                    delay || 500);
        };
    }

    return {
        throttle: throttle
    };

})();



/*******************************************************************************
 *
 * Servicios y utilidades generales a la pagina.
 *
 ******************************************************************************/
$(document).ready(function() {
});
