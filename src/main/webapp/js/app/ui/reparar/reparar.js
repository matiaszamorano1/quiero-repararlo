quierorepararlo.ui.reparar = (function() {
    function init() {
        $(".banner-bg-item").fadeTo("slow", 1);
        $('#qr-form-reparar').submit(crearReparacion);
    }

    function crearReparacion() {
        var trabajo = {};
        var usuario = {};
        var profesion = {};
        usuario.email = $('#qr-email').val().trim().toLowerCase();
        profesion.id = $('select[name=profesiones] option:selected').val();
        trabajo.usuario = usuario;
        trabajo.profesion = profesion;

        quierorepararlo.service.trabajo.guardar(trabajo)
                .done(function(data) {
                    console.log(data);
                    window.location.replace(quierorepararlo.editarTrabajoUrl(data.token));
                })
                .fail(function(e) {
                    alert("Ocurrió un error. Intentá de nuevo");
                });
        return false;
    }

    return {
        init: init
    };
})();

$(document).ready(function() {
    quierorepararlo.ui.reparar.init();
});