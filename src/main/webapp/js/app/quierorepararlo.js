var quierorepararlo = (function() {
    function url() {
        return document.baseURI;
    }
    
    function editarTrabajoUrl(token) {
        return document.baseURI + "trabajo/editar/" + token;
    }

    return {
        url: url,
        editarTrabajoUrl: editarTrabajoUrl
    };
})();