package com.mz.quierorepararlo.repository;

import com.mz.quierorepararlo.domain.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

    Usuario findByEmail(String email);
}
