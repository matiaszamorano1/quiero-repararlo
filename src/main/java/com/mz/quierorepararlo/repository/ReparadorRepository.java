
package com.mz.quierorepararlo.repository;

import com.mz.quierorepararlo.domain.Reparador;
import org.springframework.data.repository.CrudRepository;

public interface ReparadorRepository extends CrudRepository<Reparador, Long>{
    
     Reparador findByEmail(String email);
}
