package com.mz.quierorepararlo.repository;

import com.mz.quierorepararlo.domain.Trabajo;
import org.springframework.data.repository.CrudRepository;

public interface TrabajoRepository extends CrudRepository<Trabajo, Long>{

    Trabajo findByToken(String token);
}
