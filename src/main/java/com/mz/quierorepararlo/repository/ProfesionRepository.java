package com.mz.quierorepararlo.repository;

import com.mz.quierorepararlo.domain.Profesion;
import org.springframework.data.repository.CrudRepository;

public interface ProfesionRepository extends CrudRepository<Profesion, Long>{
    
}
