package com.mz.quierorepararlo.repository;

import com.mz.quierorepararlo.domain.ProfesionReparador;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ProfesionReparadorRepository  extends CrudRepository<ProfesionReparador, Long> {
    
    
    @Query("select pr.profesion.nombre, count(pr) from ProfesionReparador pr "
            + "group by pr.profesion.nombre "
            + "order by pr.profesion.nombre ")
    List<Object[]> buscarResumenReparadores();
    
}
