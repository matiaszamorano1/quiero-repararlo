package com.mz.quierorepararlo.service;

import com.mz.quierorepararlo.domain.Trabajo;

public interface TrabajoService {

    void guardar(Trabajo trabajo);

    Trabajo buscarPorToken(String token);
}
