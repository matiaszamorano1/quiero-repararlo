package com.mz.quierorepararlo.service.impl;

import com.mz.quierorepararlo.domain.Trabajo;
import com.mz.quierorepararlo.domain.Usuario;
import com.mz.quierorepararlo.repository.TrabajoRepository;
import com.mz.quierorepararlo.repository.UsuarioRepository;
import com.mz.quierorepararlo.service.TrabajoService;
import java.math.BigInteger;
import java.security.SecureRandom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TrabajoServiceImpl implements TrabajoService {

    @Autowired
    private TrabajoRepository trabajoRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public void guardar(Trabajo trabajo) {
        Usuario usuario = usuarioRepository.findByEmail(trabajo.getUsuario().getEmail());
        if (usuario != null) {
            trabajo.setUsuario(usuario);
        }
        SecureRandom random = new SecureRandom();
        trabajo.setToken(new BigInteger(130, random).toString(32));
        trabajo.setTitulo("");
        trabajo.setDescripcion("");
        trabajoRepository.save(trabajo);
    }

    @Override
    public Trabajo buscarPorToken(String token) {
        return trabajoRepository.findByToken(token);
    }

}
