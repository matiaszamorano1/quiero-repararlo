package com.mz.quierorepararlo.service.impl;

import com.mz.quierorepararlo.domain.Profesion;
import com.mz.quierorepararlo.repository.ProfesionRepository;
import com.mz.quierorepararlo.service.ProfesionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProfesionServiceImpl implements ProfesionService {
    
    @Autowired
    private ProfesionRepository profesionRepository;

    @Override
    public List<Profesion> buscarTodos() {
        return (List<Profesion>) profesionRepository.findAll();
    }

}
