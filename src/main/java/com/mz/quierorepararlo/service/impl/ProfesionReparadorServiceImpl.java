/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mz.quierorepararlo.service.impl;

import com.mz.quierorepararlo.domain.ProfesionReparador;
import com.mz.quierorepararlo.domain.Reparador;
import com.mz.quierorepararlo.repository.ProfesionReparadorRepository;
import com.mz.quierorepararlo.repository.ReparadorRepository;
import com.mz.quierorepararlo.service.ProfesionReparadorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProfesionReparadorServiceImpl implements ProfesionReparadorService {

    @Autowired
    private ProfesionReparadorRepository profesionReparadorRepository;

    @Autowired
    private ReparadorRepository reparadorRepository;

    @Override
    public void guardar(ProfesionReparador profesionReparador) {
        Reparador reparador = reparadorRepository.findByEmail(profesionReparador.getReparador().getEmail());
        if (reparador != null) {
            profesionReparador.setReparador(reparador);
        }
        profesionReparadorRepository.save(profesionReparador);
    }

}
