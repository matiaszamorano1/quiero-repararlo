package com.mz.quierorepararlo.service.impl;

import com.mz.quierorepararlo.repository.ProfesionReparadorRepository;
import com.mz.quierorepararlo.service.ReparadorService;
import com.mz.quierorepararlo.vo.ResumenReparadorVo;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ReparadorServiceImpl implements ReparadorService {

    @Autowired
    private ProfesionReparadorRepository profesionReparadorRepository;

    @Override
    public List<ResumenReparadorVo> buscarResumenReparadores() {
        List<Object[]> resumenes = profesionReparadorRepository.buscarResumenReparadores();
        List<ResumenReparadorVo> resumenesReparadorVo = new ArrayList<ResumenReparadorVo>();
        for (Object[] fila : resumenes) {
            ResumenReparadorVo resumen = new ResumenReparadorVo();
            resumen.setProfesion((String) fila[0]);
            resumen.setCantidad((Long) fila[1]);
            resumenesReparadorVo.add(resumen);
        }
        return resumenesReparadorVo;
    }

}
