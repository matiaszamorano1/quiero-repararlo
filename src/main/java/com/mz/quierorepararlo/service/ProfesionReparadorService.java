package com.mz.quierorepararlo.service;

import com.mz.quierorepararlo.domain.ProfesionReparador;

public interface ProfesionReparadorService {

    void guardar(ProfesionReparador profesionReparador);
    
}
