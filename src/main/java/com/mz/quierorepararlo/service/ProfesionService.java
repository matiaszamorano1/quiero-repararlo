package com.mz.quierorepararlo.service;

import com.mz.quierorepararlo.domain.Profesion;
import java.util.List;

public interface ProfesionService {

    List<Profesion> buscarTodos();

}
