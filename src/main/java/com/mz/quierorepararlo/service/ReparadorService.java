package com.mz.quierorepararlo.service;

import com.mz.quierorepararlo.vo.ResumenReparadorVo;
import java.util.List;

public interface ReparadorService {
    
     List<ResumenReparadorVo> buscarResumenReparadores();
}
