package com.mz.quierorepararlo.controller;

import com.mz.quierorepararlo.domain.Profesion;
import com.mz.quierorepararlo.service.ProfesionService;
import com.mz.quierorepararlo.service.ReparadorService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @Autowired
    private ProfesionService profesionService;

    @Autowired
    private ReparadorService reparadorService;

    @RequestMapping("/")
    public String home(Model model) {
        List<Profesion> profesiones = profesionService.buscarTodos();
        model.addAttribute("profesiones", profesiones);
        model.addAttribute("resumenReparadores", reparadorService.buscarResumenReparadores());
        return "home";
    }
}
