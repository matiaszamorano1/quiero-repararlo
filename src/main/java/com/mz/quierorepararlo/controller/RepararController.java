package com.mz.quierorepararlo.controller;

import com.mz.quierorepararlo.domain.Profesion;
import com.mz.quierorepararlo.service.ProfesionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RepararController {

    @Autowired
    private ProfesionService profesionService;

    @RequestMapping("/reparar")
    public String home(Model model) {
        List<Profesion> profesiones = profesionService.buscarTodos();
        model.addAttribute("profesiones", profesiones);
        return "reparar";
    }
}
