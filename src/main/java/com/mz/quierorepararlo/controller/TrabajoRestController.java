package com.mz.quierorepararlo.controller;

import com.mz.quierorepararlo.domain.Trabajo;
import com.mz.quierorepararlo.service.TrabajoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TrabajoRestController {

    @Autowired
    private TrabajoService trabajoService;

    @RequestMapping(value = "/api/trabajo", method = RequestMethod.POST)
    public Trabajo guardar(@RequestBody Trabajo trabajo) {
        trabajoService.guardar(trabajo);
        return trabajo;
    }
}
