package com.mz.quierorepararlo.controller;

import com.mz.quierorepararlo.domain.ProfesionReparador;
import com.mz.quierorepararlo.service.ProfesionReparadorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProfesionReparadorRestController {
    
    @Autowired
    private ProfesionReparadorService profesionReparadorService;

    @RequestMapping(value = "/api/profesionreparador", method = RequestMethod.POST)
    public ProfesionReparador guardar(@RequestBody ProfesionReparador profesionReparador) {
        profesionReparadorService.guardar(profesionReparador);
        return profesionReparador;
    }

}
