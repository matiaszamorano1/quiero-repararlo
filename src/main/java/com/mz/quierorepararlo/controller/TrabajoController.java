package com.mz.quierorepararlo.controller;

import com.mz.quierorepararlo.domain.Trabajo;
import com.mz.quierorepararlo.exception.ResourceNotFoundException;
import com.mz.quierorepararlo.service.TrabajoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TrabajoController {

    @Autowired
    private TrabajoService trabajoService;

    @RequestMapping("/trabajo/editar/{token}")
    public String editar(@PathVariable String token, Model model) {
        Trabajo trabajo = trabajoService.buscarPorToken(token);
        if (trabajo == null) {
            throw new ResourceNotFoundException("Trabajo no encontrado.");
        }
        model.addAttribute("trabajo", trabajo);

        return "editarTrabajo";
    }

}
