package com.mz.quierorepararlo.service;

import com.mz.quierorepararlo.QuieroRepararloAbstractTest;
import com.mz.quierorepararlo.domain.ProfesionReparador;
import com.mz.quierorepararlo.domain.Reparador;
import com.mz.quierorepararlo.repository.ProfesionRepository;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ProfesionReparadorServiceTest extends QuieroRepararloAbstractTest {

    @Autowired
    private ProfesionReparadorService profesionReparadorService;

    @Autowired
    private ProfesionRepository profesionRepository;

    @Test
    public void guardar_conReparadorNuevo_creaElReparadorYElProfesionReparador() {
        Reparador reparador = new Reparador();
        reparador.setEmail("unmail@gmail.com");

        ProfesionReparador profesionReparador = new ProfesionReparador();
        profesionReparador.setReparador(reparador);
        profesionReparador.setProfesion(profesionRepository.findOne(1L));

        profesionReparadorService.guardar(profesionReparador);

        assertNotNull(profesionReparador.getId());
        assertNotNull(reparador.getId());
    }

    @Test
    public void guardar_reparadorExistente_guardaProfesionReparador() {
        Reparador reparador = new Reparador();
        reparador.setEmail("matzam@gmail.com");

        ProfesionReparador profesionReparador = new ProfesionReparador();
        profesionReparador.setReparador(reparador);
        profesionReparador.setProfesion(profesionRepository.findOne(15L));

        profesionReparadorService.guardar(profesionReparador);

        assertNotNull(profesionReparador.getId());
    }

}
