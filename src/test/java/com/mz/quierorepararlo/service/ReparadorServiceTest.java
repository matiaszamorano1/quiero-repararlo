package com.mz.quierorepararlo.service;

import com.mz.quierorepararlo.QuieroRepararloAbstractTest;
import com.mz.quierorepararlo.vo.ResumenReparadorVo;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ReparadorServiceTest extends QuieroRepararloAbstractTest {

    @Autowired
    private ReparadorService reparadorService;

    @Test
    public void buscarResumenReparadores_conDatosValidos_devuelveElResumenConLaCantidadDeReparadoresPorProfesion() {
        List<ResumenReparadorVo> resumen = reparadorService.buscarResumenReparadores();
        assertEquals(5, resumen.size());

        assertEquals("Aires Acondicionados", resumen.get(0).getProfesion());
        assertEquals(1, resumen.get(0).getCantidad().intValue());

        assertEquals("Autos", resumen.get(1).getProfesion());
        assertEquals(1, resumen.get(1).getCantidad().intValue());

        assertEquals("Gasista", resumen.get(2).getProfesion());
        assertEquals(1, resumen.get(2).getCantidad().intValue());

        assertEquals(3, resumen.get(3).getCantidad().intValue());

        assertEquals("Televisores", resumen.get(4).getProfesion());
        assertEquals(2, resumen.get(4).getCantidad().intValue());

    }
}
