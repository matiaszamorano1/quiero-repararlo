package com.mz.quierorepararlo.service;

import com.mz.quierorepararlo.QuieroRepararloAbstractTest;
import com.mz.quierorepararlo.domain.Profesion;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ProfesionServiceTest extends QuieroRepararloAbstractTest{
    
    @Autowired
    private ProfesionService profesionService;


    @Test
    public void buscarTodos_conDatosValidos_devuelveTodasLosProfesiones() {
        List<Profesion> profesiones = profesionService.buscarTodos();
        assertEquals(20, profesiones.size());
    }
}
