package com.mz.quierorepararlo.service;

import com.mz.quierorepararlo.QuieroRepararloAbstractTest;
import com.mz.quierorepararlo.domain.Trabajo;
import com.mz.quierorepararlo.domain.Usuario;
import com.mz.quierorepararlo.repository.ProfesionRepository;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class TrabajoServiceTest extends QuieroRepararloAbstractTest {

    @Autowired
    private TrabajoService trabajoService;

    @Autowired
    private ProfesionRepository profesionRepository;

    @Test
    public void guardar_conTrabajoConUsuarioNuevo_creaTrabajoYUsuario() {
        Trabajo trabajo = new Trabajo();
        trabajo.setTitulo("Un titulo");
        trabajo.setDescripcion("Una descripcion");

        Usuario usuario = new Usuario();
        usuario.setEmail("unemail@gmail.com");
        trabajo.setUsuario(usuario);

        trabajo.setProfesion(profesionRepository.findOne(1L));

        trabajoService.guardar(trabajo);

        assertNotNull(trabajo.getId());
        assertNotNull(usuario.getId());

    }

    @Test
    public void guardar_trabajoConUsuarioExistente_guardaElTrabajo() {
        Trabajo trabajo = new Trabajo();
        trabajo.setTitulo("Un titulo");
        trabajo.setDescripcion("Una descripcion");

        Usuario usuario = new Usuario();
        usuario.setEmail("matzam@gmail.com");
        trabajo.setUsuario(usuario);

        trabajo.setProfesion(profesionRepository.findOne(1L));

        trabajoService.guardar(trabajo);

        assertNotNull(trabajo.getId());
    }

    @Test
    public void guardar_trabajoNuevo_generaToken() {
        Trabajo trabajo = new Trabajo();
        trabajo.setTitulo("Un titulo");
        trabajo.setDescripcion("Una descripcion");

        Usuario usuario = new Usuario();
        usuario.setEmail("unemail@gmail.com");
        trabajo.setUsuario(usuario);

        trabajo.setProfesion(profesionRepository.findOne(1L));

        trabajoService.guardar(trabajo);

        assertNotNull(trabajo.getToken());
    }

    @Test
    public void guardar_sinTituloNiDescripcion_guardaIgualConTextoDummy() {
        Trabajo trabajo = new Trabajo();

        Usuario usuario = new Usuario();
        usuario.setEmail("unemail@gmail.com");
        trabajo.setUsuario(usuario);

        trabajo.setProfesion(profesionRepository.findOne(1L));

        trabajoService.guardar(trabajo);

        assertNotNull(trabajo.getId());
    }

    @Test
    public void buscarPorToken_conToken_devuelveElTrabajo() {
        Trabajo trabajo = trabajoService.buscarPorToken("123456");
        assertNotNull(trabajo);
    }
}
