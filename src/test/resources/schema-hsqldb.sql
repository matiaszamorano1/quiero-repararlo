DROP TABLE IF EXISTS profesion_reparador;
DROP TABLE IF EXISTS trabajo;
DROP TABLE IF EXISTS reparador;
DROP TABLE IF EXISTS profesion;
DROP TABLE IF EXISTS usuario;

CREATE TABLE usuario (
  id BIGINT IDENTITY PRIMARY KEY,
  email VARCHAR(100) NOT NULL UNIQUE
);

CREATE TABLE reparador (
  id BIGINT IDENTITY PRIMARY KEY,
  email VARCHAR(100) NOT NULL UNIQUE
);

CREATE TABLE  profesion (
  id BIGINT IDENTITY PRIMARY KEY,
  nombre VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE profesion_reparador (
    id BIGINT IDENTITY PRIMARY KEY,
    id_reparador BIGINT NOT NULL,
    id_profesion BIGINT NOT NULL,
    FOREIGN KEY (id_reparador) REFERENCES reparador(id),
    FOREIGN KEY (id_profesion) REFERENCES profesion(id),
    UNIQUE (id_profesion, id_reparador)
);


CREATE TABLE trabajo (
    id BIGINT IDENTITY PRIMARY KEY,
    titulo VARCHAR(255) NOT NULL,
    descripcion VARCHAR(3000) NOT NULL,
    token VARCHAR(255) NOT NULL UNIQUE,
    id_usuario BIGINT NOT NULL,
    id_profesion BIGINT NOT NULL,
    FOREIGN KEY (id_usuario) REFERENCES usuario(id),
    FOREIGN KEY (id_profesion) REFERENCES profesion(id)
);


INSERT INTO profesion (id, nombre) VALUES
(1, 'Autos'),
(2, 'Cámaras'),
(3, 'Carpintería'),
(4, 'Celulares'),
(5, 'Computadoras'),
(6, 'Consolas'),
(7, 'Electricidad'),
(8, 'Aires Acondicionados'),
(9, 'Electrodomésticos'),
(10, 'Electrónica'),
(11, 'Gasista'),
(12, 'Hogar'),
(13, 'Impresoras'),
(14, 'Instrumentos'),
(15, 'Monitores'),
(16, 'Motos'),
(20, 'Otro'),
(17, 'Pintor'),
(18, 'Plomería'),
(19, 'Televisores');

INSERT INTO reparador (id, email) VALUES
    (1, 'matzam@gmail.com'),
    (2, 'plomero@gmail.com'),
    (3, 'plomero2@gmail.com'),
    (4, 'gasista@gmail.com'),
    (5, 'aires@gmail.com'),
    (6, 'televisores@gmail.com');

INSERT INTO usuario(id, email) VALUES
    (1, 'matzam@gmail.com');

INSERT INTO profesion_reparador (id_reparador, id_profesion) VALUES
    (1, 1),
    (1, 18),
    (1, 19),
    (2, 18),
    (3, 18),
    (4, 11),
    (5, 8),
    (6, 19);

INSERT INTO trabajo (id, titulo, descripcion, token, id_usuario, id_profesion) VALUES
    (1, 'microndas', 'una descripcion', '123456', 1, 1);